#!/bin/bash 

clear
[ $(whoami) != "root" ] && echo "Ejecutar como root" && exit

echo INSTALACION DNKBASH

echo ----------------------------------------
apt-get update >/dev/null 2>/dev/null && echo "apt-get update --> OK" || echo "apt-get update --> KO !!!!!!"
echo ----------------------------------------
apt-get -y install vim >/dev/null 2>/dev/null && echo "vim --> OK" || echo "vim --> KO !!!!!!"
echo ----------------------------------------
test -f /root/.bashrc && mv /root/.bashrc /root/.bashrc.old
echo ----------------------------------------
wget "https://gitlab.com/akalonso/dnkbash/-/raw/main/vimrc" -O /etc/dnkbash >/dev/null 2>/dev/null && echo "bash.bashrc bajado copiado en /etc" || echo "No se ha podido bajar el bash.bashrc!!!!!!"
echo ----------------------------------------
#añadimos el source en el bash.bashrc
test "`grep dnkbash /etc/bash.bashrc`" || echo "source /etc/dnkbash" >> /etc/bash.bashrc
#comprobamos que este añadido
test "`grep dnkbash /etc/bash.bashrc`" || echo "dnkbash esta presente en /etc/bash.bashrc"

#añadimos el source en /etc/profile
test "`grep dnkbash /etc/profile`" || echo "source /etc/dnkbash" >> /etc/profile
#comprobamos que este añadido
test "`grep dnkbash /etc/profile`" || echo "dnkbash esta presente en /etc/profile"
echo ----------------------------------------
cp /etc/vim/vimrc /etc/vim/vimrc.old
wget "https://gitlab.com/akalonso/dnkbash/-/raw/main/vimrc" -O /etc/vim/vimrc >/dev/null 2>/dev/null && echo "vimrc bajado y copiado en /etc/vim" || echo "No se ha podido bajar el vimrc!!!!!!"
echo ----------------------------------------
/bin/bash
