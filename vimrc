" para vim-6.1 o compatible     "
"-------------------------------"

" vim version
version 6.1

" {{{ Opciones generales "
"------------------------"
"set number
set nocompatible
set showcmd        " Show (partial) command in status line.
set showmatch      " Show matching brackets.
set ruler          " Show the line and column numbers of the cursor
set ignorecase     " Do case insensitive matching
set incsearch      " Incremental search
set hlsearch       " highlight last search matches
set nobackup       " Don't keep a backup file
set textwidth=0     " Don't wrap words by default
set visualbell t_vb=
set history=50
set undolevels=100
set guifont=7x13bold
set viminfo='20,\"50
set history=50
set cmdheight=1     " ¿2 lines for messages (fewer "Press Return" messages)?
"set showbreak=+     " prefix wrapped lines with a + sign
set wildmenu        " use a menu for wildcard completion
" Suffixes that get lower priority when doing tab completion for filenames.
" These are files we are not likely to want to edit or read.
set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.i+lg,.inx,.out,.toc
" We know xterm-debian is a color terminal
if &term =~ "xterm-debian" || &term =~ "xterm-xfree86"
  set t_Co=16
  set t_Sf=^[[3%dm
  set t_Sb=^[[4%dm
endif
" folders {{{ data }}} :)
set foldenable
set foldmethod=marker
"set foldmethod=syntax
"set foldlevel=1000
"set foldmethod=indent
"set foldlevel=1
"set foldclose=all

" limit memory usage
set maxmem=8192     " 8MB max per buffer
set maxmemtot=32768 " 32MB total max

" Selecting text in visual mode and hitting > indent text but also
" deselect text.  Following remap will make sure text remains selected
vnoremap > ><CR>gv
vnoremap < <<CR>gv

nnoremap <c-g> 2<c-g> " make <c-g> more verbose

" suppress highlight of last search when hitting return
nnoremap <silent> <cr> :noh<cr>

" no borrar pantalla al salir (clear)
" si se usa, no va la rueda del raton con vi
"set t_ti= t_te=
"}}}

" {{{ Opciones de tabulacion "
"----------------------------"
set noexpandtab    " no convierte tabs a espacios
set tabstop=4
set smarttab
set shiftwidth=4
set shiftround
set showmode
set backspace=indent,eol,start
set autoindent
set smartindent			" para borrar tabs que son espacios
                         " upper case letters in pattern
set shortmess=at           " avoid 'hit Return' messages
set wildchar=<TAB> " command expansion key a la tcsh
set wildmode=list:longest,full  " show all matches, expand to longest partial,
                                " and then cycle through choices with tab
"show tabs, spaces, end of lines
set list
"set listchars=tab:\│\ ,trail:_,extends:>,precedes:<,nbsp:%
"set listchars=tab:\|\ ,trail:_,extends:>,precedes:<,nbsp:%
"set listchars=tab:▶─,trail:_,extends:>,precedes:<,nbsp:%

set listchars=tab:_.,trail:_
"set listchars=tab:>-,trail:_,extends:>,precedes:<,nbsp:%
"set listchars=tab:▶─,trail:_,extends:>,precedes:<,nbsp:%
"set listchars=tab:▹─,trail:⏘,extends:>,precedes:<,nbsp:%
"set listchars=tab:▶─,trail:⏑,extends:>,precedes:<,nbsp:%


"set listchars=tab:\ \ ,trail:·
"}}}
"

" {{{ Syntax Hightlighting "
"--------------------------"

function! DnkTheme()
    syntax on
    set background=dark
    "syntax reset
    "let g:colors_name = "dnk"
    "let g:colors_name = "none"
	hi Normal  ctermbg=none
    "hi Normal     ctermbg=none guibg=#000000
    "hi Normal     ctermbg=black guibg=#000000
	"hi Normal		ctermfg=gray  		   guifg=#acaeac gui=none
    hi Comment	ctermfg=94   guifg=#bd6500 gui=none
    hi PreProc	ctermfg=darkmagenta	guifg=#cd00cd gui=none
    hi Constant	ctermfg=darkgreen    guifg=#009900 gui=none
    hi Special    ctermfg=green		guifg=#00ff00 gui=none
    hi Type 		ctermfg=darkcyan     guifg=#00cacd gui=none
    hi Identifier ctermfg=darkred    guifg=#cc0000 gui=none
    hi Statement ctermfg=cyan      guifg=#00ffff gui=none
    "hi Statement ctermfg=darkred guifg=#cc0000 gui=none
    hi Groupstart	ctermfg=darkblue guifg=#000099 gui=none
	hi WarningMsg	ctermfg=magenta		guifg=#ff00ff gui=none

    hi Search 		ctermfg=darkred  ctermbg=yellow guifg=#ffffff gui=none
    hi Todo 		ctermfg=magenta			guifg=#ff00ff gui=none
    hi Todo 		ctermbg=black			guibg=#000000

    hi Error		ctermfg=red  			guifg=#ff0000
    hi Error		ctermbg=black  		guibg=#000000 gui=none

    hi clear visual
    "hi Visual     ctermfg=gray         guifg=#acaeac
    "hi visual     ctermbg=darkblue     guibg=#3130bd
	hi visual ctermbg=239

    hi modemsg    ctermfg=white        guifg=#ffffff
    hi modemsg    ctermbg=black        guibg=#000000 gui=none

    hi folded     ctermfg=166      guifg=#ffff00
    hi folded     ctermbg=234        guibg=#000000 gui=none

    hi question   gui=none

    hi MatchParen ctermbg=0 ctermfg=yellow

	"match EndSpaces /\ \+$/
    "hi Tabs ctermfg=232
	"match Tabs /\t\+/
    "hi nontext ctermfg=236
	"match NonText '^\s\+'

	" completaod
	hi Pmenu ctermbg=darkblue ctermfg=gray
	hi PmenuSel ctermbg=blue ctermfg=white

	" IndentGuides
	"hi IndentGuidesOdd ctermbg=233 ctermfg=232
	"hi IndentGuidesEven ctermbg=232 ctermfg=233

	"hi CursorLine cterm=none ctermfg=none ctermbg=235
	"hi CursorColumn cterm=none ctermfg=none ctermbg=234
	
	" espacios al final de linea
    hi EndSpaces ctermfg=52
    "hi EndSpaces ctermfg=52 ctermbg=233
	match EndSpaces / \+$/
	"match EndSpaces /[\t ]\+$/
	
	hi CursorLine cterm=none ctermfg=none ctermbg=0
	hi CursorColumn cterm=none ctermfg=none ctermbg=0
	hi LineNr ctermbg=232 ctermfg=darkgrey
	hi CursorLineNr ctermbg=235 ctermfg=yellow
	
	" tabs y espacios
    "hi specialkey ctermfg=232     guifg=#525052 gui=none
	hi specialkey ctermfg=234
	
	" truco para colorear los tabs sobre el cursorline (desactiva endspaces)
	"hi NonText cterm=none ctermfg=234 ctermbg=none
	"match NonText '^\t\+'

endfunction

if has("syntax")
	call DnkTheme()
endif
"}}}

" {{{ Empezar en la ultima posicion "
"-----------------------------------"
if has("autocmd")
   augroup LastPos
      au!
      autocmd BufReadPost *
      \ if line("'\"") > 0 && line("'\"") <= line("$") |
      \   exe "normal g`\"" |
      \ endif
   augroup END
endif
"}}}

" {{{ Status Line :) "
"--------------------"
if ! exists("no_plugin_maps")
	if has("statusline")
		set laststatus=2
		if has("eval")
			" Vim can read compressed files, so we use this function to display the
			" compression mode in the statusline
			function! CompressFlag()
				let l:name = bufname("")
				if l:name =~'.gz$'
					return '[GZ]'
				elseif l:name =~ '.bz2$'
					return '[BZ2]'
				elseif l:name =~ '.Z$'
					return '[Z]'
				else
					return ''
				endif
			endfunction
			set statusline=%1*(%n)\ %3*%<%F\ %1*%y%2*%m%1*%r%{CompressFlag()}\ %=\ %3*\[%{''.(&fenc!=''?&fenc:&enc).''},%{(&bomb?\",BOM\":\"\")}%{&ff}\]\ %1*%l,%-8(%c%V%)%{VimBuddy()}\ \ %P
		else
			set statusline=%1*(%n)\ %3*%<%F\ %1*%y%2*%m%1*%r%=%l,%-8(%c%V%)%{VimBuddy()}\ \ %P
		endif
		hi User1 ctermfg=darkBlue  ctermbg=LightGray  guifg=#3130bd  guibg=#acaeac
		hi User2 ctermfg=darkred   ctermbg=LightGray  guifg=#cC0000  guibg=#acaeac
		hi User3 ctermfg=black     ctermbg=LightGray  guifg=#000000  guibg=#acaeac
	endif
endif
"}}}

" {{{ Control de equivocaciones comunes :O "
"------------------------------------------"
"cab Q q
"cab W w
"cab X x
"ret 3
command! W  write
command! Wq wq
cab q1 q!
cab Q! q!
"}}}

" {{{ Teclas de funcion y otros aliases
" F2 = save
nmap <ESC>[12~           :w<C-M>
imap <ESC>[12~      <C-O>:w<C-M>

" Shift F4 = cierra buffer
nmap [26~ :bd<C-M>
imap [26~ <C-O>:bd<C-M>
"nmap <S-ESC>[13~      :bd<C-M>
"imap <S-ESC>[13~ <C-O>:bd<C-M>

" F4/F16 = salir
"nmap <F4> :qa<C-M>
"imap <F4> <ESC>:qa<C-M>
nmap <ESC>[14~ :qa!<C-M>
imap <ESC>[14~ <ESC>:qa!<C-M>
nmap <ESC>[29~ :qa!<c-M>
imap <ESC>[29~ <ESC>:qa!<C-M>

" F9 = grabar / make install
nmap <F9> :w<CR>:make install<CR>
imap <F9> <C-O>:w<CR><C-O>:make install<CR>

" F5 anterior fichero
nmap <F5> :bp<CR>
imap <F5> <ESC>:bp<CR>

" F6 siguiente fichero
nmap <F6> :bn<CR>
imap <F6> <ESC>:bn<CR>

" F7 abre/cierra folder
nmap <F7> za<CR>
imap <F7> <C-O>za
"map <S-F7> zi<CR>
"imap <A-F7> <C-O>zc<CR>

" F10 codificación iso8859-1
nmap <F10> :set fileencoding=latin1<CR>
imap <F10> <ESC>:set fileencoding=latin1<CR>

" F11 codificación utf8
nmap <F11> :set fileencoding=utf8<CR>
imap <F11> <ESC>:set fileencoding=utf8<CR>

" F12 desactiva/activa folders
"nmap <F12> zi<CR>
"imap <F12> <C-O>zi
nmap <F12> :q<CR>
imap <F12> <C-O>:q<CR>

" F8 paste/nopaste
"set pastetoggle=<F8>
"map <ESC>[19~ :set list!<CR>:set paste!<CR>:set paste?<CR>
"imap <ESC>[19~ <C-O>:set list!<CR><C-O>:set paste!<CR><C-O>:set paste?<CR>
map <ESC>[19~ :set list!<CR>:set smartindent!<CR>:set autoindent!<cr>:set nu!<cr>
imap <ESC>[19~ <C-O>:set list!<CR><C-O>:set smartindent!<CR><C-O>:set autoindent!<cr>:set nu!<cr>

" Ctrl+F8 borra espacios finales
nmap <ESC>[19^ m':%s/\ \+$//<CR>''
imap <ESC>[19^ <ESC>m':%s/\ \+$//<CR>''<INS>
" codigo tecla gnome-terminal
nmap <ESC>[19;5~ m':%s/\ \+$//<CR>''
imap <ESC>[19;5~ <ESC>m':%s/\ \+$//<CR>''<INS>
"imap <ESC>[19;5~ <C-O>m'1GvG><v:%s/\ \+$//<CR>'<ESC><CR>

" Ctrl+up (gnome-terminal) = up
map <ESC>O5A <UP>
imap <ESC>O5A <UP>
map <ESC>O5B <DOWN>
imap <ESC>O5B <DOWN>

"map <ESC>[19~ :call Paste()<CR>
"imap <ESC>[19~ <C-O>:call Paste()<CR>

" Ctrl+F8 borra espacios finales
nmap <ESC>[19^ m'1GvG><v:%s/\ *$//<CR>''
imap <ESC>[19^ <C-O>m'1GvG><v:%s/\ *$//<CR>''

" shift insert
"nmap <S-Insert> :set paste<CR>"*p:set nopaste<CR>`]
"imap <S-Insert> <ESC>:set paste<CR>"*p:set nopaste<CR>`]i

" exit rápido
noremap q :qa<CR>
"}}}

"let pasteset = 0
"function! Paste ()
"	if pasteset = 1
"		nopasteset
"		let pasteset = 0
"	else
"		pasteset
"		let pasteset = 0
"	fi
"endfunction

" {{{ plantillas "
"----------"
iab dnk· ----===( Danky )===----
iab html· <html><CR><head><CR><title ></title><CR></head><CR><body><CR><CR></body><CR></html><ESC>5kli
iab c· #include <stdlib.h><CR>#include <stdio.h><CR><CR>int main(void)<CR>{<CR><CR>return 0;<CR>}<ESC>2ki<TAB>
"iab dnk Danky
"}}}

" {{{ VimBuddy :) "
" ----------------"
" Description: VimBuddy statusline character
" Author:      Flemming Madsen <fma@cci.dk>
" Modified:    June 2001
" Version:     0.9.1
"
" Usage:       Insert %{VimBuddy()} into your 'statusline'
"

function! VimBuddy()
    " Take a copy for others to see the messages
    if ! exists("g:vimbuddy_msg")
        let g:vimbuddy_msg = v:statusmsg
    endif
    if ! exists("g:vimbuddy_warn")
        let g:vimbuddy_warn = v:warningmsg
    endif
    if ! exists("g:vimbuddy_err")
        let g:vimbuddy_err = v:errmsg
    endif
    if ! exists("g:vimbuddy_onemore")
        let g:vimbuddy_onemore = ""
    endif

    if ( exists("g:actual_curbuf") && (g:actual_curbuf != bufnr("%")))
        " Not my buffer, sleeping
        return "|-o"
    elseif g:vimbuddy_err != v:errmsg
        let v:errmsg = v:errmsg . " "
        let g:vimbuddy_err = v:errmsg
        return ":-("
    elseif g:vimbuddy_warn != v:warningmsg
        let v:warningmsg = v:warningmsg . " "
        let g:vimbuddy_warn = v:warningmsg
        return "(-:"
    elseif g:vimbuddy_msg != v:statusmsg
        let v:statusmsg = v:statusmsg . " "
        let g:vimbuddy_msg = v:statusmsg
        let test = matchstr(v:statusmsg, 'lines *$')
        let num = substitute(v:statusmsg, '^\([0-9]*\).*', '\1', '') + 0
        " How impressed should we be
        if test != "" && num > 20
            let str = ":-O"
        elseif test != "" && num
            let str = ":-o"
        else
            let str = ":-D"
        endif
		  let g:vimbuddy_onemore = str
		  return str
	 elseif g:vimbuddy_onemore != ""
		let str = g:vimbuddy_onemore
		let g:vimbuddy_onemore = ""
		return str
    endif

    if ! exists("b:lastcol")
        let b:lastcol = col(".")
    endif
    if ! exists("b:lastlineno")
        let b:lastlineno = line(".")
    endif
    let num = b:lastcol - col(".")
    let b:lastcol = col(".")
    if (num == 1 || num == -1) && b:lastlineno == line(".")
        " Let VimBuddy rotate
        let num = b:lastcol % 4
        if num == 0
            let ch = '|'
         elseif num == 1
            let ch = '/'
        elseif num == 2
            let ch = '-'
        else
            let ch = '\'
        endif
        return ":" . ch . ")"
    endif
    let b:lastlineno = line(".")

    " Happiness is my favourite mood
    return ":-)"
endfunction
"}}}

"* {{{ Settings especificos ---------------------------------------------------"
if has("eval")

" Define CompatSetLocal, using setlocal if available (Vim >=6.0)
if version < 600
   command! -nargs=+ CompatSetLocal set <args>
else
   command! -nargs=+ CompatSetLocal setlocal <args>
endif

" {{{ Makefiles Settings "
"------------------------"
function! MyMakefileSettings()
   CompatSetLocal textwidth=0
   CompatSetLocal noexpandtab
   CompatSetLocal tabstop=8
   CompatSetLocal shiftwidth=8
   CompatSetLocal list
   CompatSetLocal listchars=tab:»·,trail:·

   if has("folding")
       CompatSetLocal foldmethod=indent
       CompatSetLocal foldlevel=1000
   endif
endfunction
"}}}

" {{{ HTML Settings "
"-------------------"
function! MyHTMLSettings()
   CompatSetLocal textwidth=78
   "CompatSetLocal expandtab
   CompatSetLocal tabstop=4
   CompatSetLocal shiftwidth=4
   CompatSetLocal shiftround
   CompatSetLocal smarttab

   if has("folding")
       CompatSetLocal foldmethod=indent
       CompatSetLocal foldlevel=1000
   endif
endfunction
"}}}

" {{{ C Settings "
"----------------"
function! MyCSettings()
   " finds the current function in C
   map ,f mk[[?^[A-Za-z0-9_].*(<CR>V"ky`k:echo "<C-R>k"<CR>
   "  better version for C, doesn't always work though 8)
   map ,k  :?^{??^\k? mark k\|echo getline("'k")<cr>
   " C programming stuff
   imap ·vmv void main(void) {<CR>}<Esc>ko
   imap ·imi int main(int argc, char *argv[]) {<CR>}<Esc>ko
   imap ·if if () {<CR>}<Esc>kf(a
   imap ·inc #include <.h><ESC>2hi
   imap ·def #define
   imap ·for for (;;) {<CR>}<Esc>kf(a
   CompatSetLocal textwidth=72   " disable line wraps
   CompatSetLocal noexpandtab    " expand tabs to spaces
   CompatSetLocal tabstop=4
   CompatSetLocal shiftwidth=4
   CompatSetLocal shiftround
   CompatSetLocal smarttab
   CompatSetLocal nocindent
   CompatSetLocal cinoptions=:0,(0,u0
   "CompatSetLocal comments=sr:/*,mb:*,elx:*/,://
   CompatSetLocal comments=
   CompatSetLocal formatoptions=croql
   CompatSetLocal keywordprg=man\ -S\ 3:2
   "CompatSetLocal list
   "CompatSetLocal listchars=tab:»·,trail:·

   "if has("folding")
   "    CompatSetLocal foldmethod=indent
   "    CompatSetLocal foldlevel=1000
   "endif
endfunction
"}}}

nmap <silent><unique> <Leader>c :call Code()
function! Code()
	" \ig
	" <Plug>IndentGuidesToggle
    "hi Normal  ctermbg=016 guibg=#000000
    "hi Normal  ctermbg=none
	"call DnkTheme()
	"let g:indent_guides_auto_colors = 0
	"IndentGuidesEnable
	"hi IndentGuidesOdd ctermbg=232 ctermfg=016
	"hi IndentGuidesEven ctermbg=233 ctermfg=232
    "hi Statement ctermfg=darkred guifg=#cc0000 gui=none
	set number
	"hi LineNr ctermbg=234 ctermfg=darkgrey
	"hi CursorLineNr ctermbg=darkgrey ctermfg=yellow
	set list
endfunction

nmap <silent><unique> <Leader>p :call Paste()
function! Paste ()
	IndentGuidesDisable
	"set nonumber
	set paste
	set nolist
endfunction

" {{{ Perl Settings "
"-------------------"
function! MyPerlSettings()
   if !did_filetype()
      CompatSetLocal filetype=perl
   endif
   CompatSetLocal textwidth=72   " disable line wraps
   CompatSetLocal noexpandtab
   CompatSetLocal tabstop=4
   CompatSetLocal shiftwidth=4
   CompatSetLocal smarttab
   CompatSetLocal nocindent
   "CompatSetLocal comments=:#
   CompatSetLocal comments=
   CompatSetLocal formatoptions=croql
   CompatSetLocal keywordprg=man\ -S\ 3

   if has("quickfix")
       CompatSetLocal makeprg=$HOME/bin/vimparse.pl\ %\ $*
       CompatSetLocal errorformat=%f:%l:%m
   endif

   if has("folding")
      " CompatSetLocal foldmethod=indent
	  "CompatSetLocal foldlevel=1000
   endif
endfunction
"}}}

endif " has("eval")
"}}} End settings especificos -------------------------------------------------"

" {{{ Autocmd settings ---------------------------------------------------------
if has("autocmd")
   " Enable file type detection.
   " Use the default filetype settings, so that mail gets 'tw' set to 72,
   " 'cindent' is on in C files, etc.
   " Also load indent files, to automatically do language-dependent indenting.
   if version >= 600
      filetype plugin indent on
   else
      filetype on
   endif

   " set options based on filetype
   if has("eval")
      augroup SetEditOpts
         au!
         "autocmd FileType c :call MyCSettings()
         "autocmd FileType cpp :call MyCSettings()
         autocmd FileType c :call Code()
         autocmd FileType cpp :call Code()
         autocmd FileType perl :call MyPerlSettings()
         autocmd FileType make :call MyMakefileSettings()
         autocmd FileType html :call MyHTMLSettings()
      augroup END
   endif

   augroup misc
      au!
      autocmd BufNewFile,BufReadPost ChangeLog,Changelog
      \ nmap  0i<CR><ESC>k:.! date<CR>$a<CR>
   augroup END

   " VIM >=6.0 has a gzip plugin, so this is not needed
   if version < 600
      " Also, support editing of gzip-compressed files. DO NOT REMOVE THIS!
      " This is also used when loading the compressed helpfiles.
      augroup gzip
         " Remove all gzip autocommands
         au!

         " Enable editing of gzipped files
         "       read: set binary mode before reading the file
         "             uncompress text in buffer after reading
         "      write: compress file after writing
         "     append: uncompress file, append, compress file
         autocmd BufReadPre,FileReadPre      *.gz set bin
         autocmd BufReadPre,FileReadPre      *.gz let ch_save = &ch|set ch=2
         autocmd BufReadPost,FileReadPost    *.gz '[,']!gunzip
         autocmd BufReadPost,FileReadPost    *.gz set nobin
         autocmd BufReadPost,FileReadPost    *.gz let &ch = ch_save|unlet ch_save
         " autocmd BufReadPost,FileReadPost    *.gz execute ":doautocmd BufReadPost " . %:r

         autocmd BufWritePost,FileWritePost  *.gz !mv <afile> <afile>:r
         autocmd BufWritePost,FileWritePost  *.gz !gzip <afile>:r

         autocmd FileAppendPre               *.gz !gunzip <afile>
         autocmd FileAppendPre               *.gz !mv <afile>:r <afile>
         autocmd FileAppendPost              *.gz !mv <afile> <afile>:r
         autocmd FileAppendPost              *.gz !gzip <afile>:r
      augroup END

      augroup bzip2
         " Remove all bzip2 autocommands
         au!

         " Enable editing of bzipped files
         "       read: set binary mode before reading the file
         "             uncompress text in buffer after reading
         "      write: compress file after writing
         "     append: uncompress file, append, compress file
         autocmd BufReadPre,FileReadPre      *.bz2 set bin
         autocmd BufReadPre,FileReadPre      *.bz2 let ch_save = &ch|set ch=2
         autocmd BufReadPost,FileReadPost    *.bz2 set cmdheight=2|'[,']!bunzip2
         autocmd BufReadPost,FileReadPost    *.bz2 set cmdheight=1 nobin|execute ":doautocmd BufReadPost " . %:r
         autocmd BufReadPost,FileReadPost    *.bz2 let &ch = ch_save|unlet ch_save
         " autocmd BufReadPost,FileReadPost    *.bz2 execute ":doautocmd BufReadPost " . %:r

         autocmd BufWritePost,FileWritePost  *.bz2 !mv <afile> <afile>:r
         autocmd BufWritePost,FileWritePost  *.bz2 !bzip2 <afile>:r

         autocmd FileAppendPre               *.bz2 !bunzip2 <afile>
         autocmd FileAppendPre               *.bz2 !mv <afile>:r <afile>
         autocmd FileAppendPost              *.bz2 !mv <afile> <afile>:r
         autocmd FileAppendPost              *.bz2 !bzip2 -9 --repetitive-best <afile>:r
      augroup END

   endif " version < 600

endif
"}}} end autocmd settings -----------------------------------------------------

" {{{ VimPager "
function! VimPager()
" Vim script to work like "less"
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Modificaciones por Danky

" If not reading from stdin, skip files that can't be read.
" Exit if there is no file at all.
if argc() > 0
  let s:i = 0
  while 1
    if filereadable(argv(s:i))
      if s:i != 0
	sleep 3
      endif
      break
    endif
    if isdirectory(argv(s:i))
      echomsg "Skipping directory " . argv(s:i)
    elseif getftime(argv(s:i)) < 0
      echomsg "Skipping non-existing file " . argv(s:i)
    else
      echomsg "Skipping unreadable file " . argv(s:i)
    endif
    echo "\n"
    let s:i = s:i + 1
    if s:i == argc()
      quit
    endif
    next
  endwhile
endif

set nocp
set so=100
set hlsearch
set incsearch
nohlsearch

" Used after each command: put cursor at end and display position
"if &wrap
  "noremap <SID>L L0:file<CR>
  "au VimEnter * normal L0
"else
  "noremap <SID>L Lg0:file<CR>
  "noremap <SID>L Lg0:file<CR>
  "au VimEnter * normal Lg0
"endif

noremap <SID>L :file<CR>
au VimEnter * normal L

" When reading from stdin don't consider the file modified.
au VimEnter * set nomod

" Can't modify the text
set noma

" Give help
noremap h :call <SID>Help()<CR>
map H h
fun! s:Help()
  echo "<Space>   One page forward          b         One page backward"
  echo "d         Half a page forward       u         Half a page backward"
  echo "<Enter>   One line forward          k         One line backward"
  echo "G         End of file               g         Start of file"
  echo "N%        percentage in file"
  echo "\n"
  echo "/pattern  Search for pattern"
  echo "n         next pattern match        N         Previous pattern match"
  echo "\n"
  echo ":n<Enter> Next file                 :p<Enter> Previous file"
  echo "\n"
  echo "q         Quit                      v         Edit file"
  let i = input("Hit Enter to continue")
endfun

" Scroll one page forward
noremap <script> <Space> :call <SID>NextPage()<CR><SID>L
map <C-V> <Space>
map f <Space>
map <C-F> <Space>
map z <Space>
map <Esc><Space> <Space>
fun! s:NextPage()
  if line(".") == line("$")
    if argidx() + 1 >= argc()
      quit
    endif
    next
    1
  else
    exe "normal! \<C-F>"
  endif
endfun

" Re-read file and page forward "tail -f"
map F :e<CR>G<SID>L:sleep 1<CR>F

" Scroll half a page forward
noremap <script> d <C-D><SID>L
map <C-D> d

" Scroll one line forward
noremap <script> j <C-E><SID>L
map <C-N> j
map e j
map <C-E> j
map <C-J> j
nnoremap <silent> <cr> :noh<cr>j

" Scroll one page backward
noremap <script> b <C-B><SID>L
map <C-B> b
map w b
map <Esc>v b

" Scroll half a page backward
noremap <script> u <C-U><SID>L
noremap <script> <C-U> <C-U><SID>L

" Scroll one line backward
noremap <script> k <C-Y><SID>L
map y k
map <C-Y> k
map <C-P> k
map <C-K> k

" Redraw
noremap <script> r <C-L><SID>L
noremap <script> <C-R> <C-L><SID>L
noremap <script> R <C-L><SID>L

" Start of file
noremap <script> g gg<SID>L
map < g
map <Esc>< g

" End of file
noremap <script> G G<SID>L
map > G
map <Esc>> G

" Go to percentage
noremap <script> % %<SID>L
map p %

" Next pattern
noremap <script> n n<SID>L
noremap <script> N N<SID>L

" Quitting
noremap q :q<CR>

" Switch to editing (switch off less mode)
map v :call <SID>End()<CR>
fun! s:End()
  set ma
  unmap h
  unmap H
  unmap <Space>
  unmap <C-V>
  unmap f
  unmap <C-F>
  unmap z
  unmap <Esc><Space>
  unmap F
  unmap d
  unmap <C-D>
  unmap <CR>
  unmap <C-N>
  unmap e
  unmap <C-E>
  unmap j
  unmap <C-J>
  unmap b
  unmap <C-B>
  unmap w
  unmap <Esc>v
  unmap u
  unmap <C-U>
  unmap k
  unmap y
  unmap <C-Y>
  unmap <C-P>
  unmap <C-K>
  unmap r
  unmap <C-R>
  unmap R
  unmap g
  unmap <
  unmap <Esc><
  unmap G
  unmap >
  unmap <Esc>>
  unmap %
  unmap p
  unmap n
  unmap N
  unmap q
  unmap v
endfun

" dnk
set ts=8
set nolist
set ignorecase
"set nomodifiable

map <up> k
map <down> j
map <PageUp> u
map <kPageUp> u
map <PageDown> d
map <kPageDown> d
map <Esc> q
set laststatus=1 " no status line
"set statusline=%1*(%n)\ %3*%f\ %1*%y%2*%m%1*%r%=%l,%-8(%c%V%)%{VimBuddy()}+\ \ %P
set nofoldenable

endfunction
" }}}

" {{{ GnuPG
" Transparent editing of gpg encrypted files.
" By Wouter Hanegraaff <wouter@blub.net>
augroup encrypted
   au!
   " First make sure nothing is written to ~/.viminfo while editing
   " an encrypted file.
   autocmd BufReadPre,FileReadPre      *.gpg set viminfo=
   " We don't want a swap file, as it writes unencrypted data to disk
   autocmd BufReadPre,FileReadPre      *.gpg set noswapfile
   " Switch to binary mode to read the encrypted file
   autocmd BufReadPre,FileReadPre      *.gpg set bin
   autocmd BufReadPre,FileReadPre      *.gpg let ch_save = &ch|set ch=2
   autocmd BufReadPost,FileReadPost    *.gpg '[,']!gpg -qd
   " Switch to normal mode for editing
   autocmd BufReadPost,FileReadPost    *.gpg set nobin
   autocmd BufReadPost,FileReadPost    *.gpg let &ch = ch_save|unlet ch_save
   autocmd BufReadPost,FileReadPost    *.gpg execute ":doautocmd BufReadPost " . expand("%:r")

   " Convert all text to encrypted text before writing
   autocmd BufWritePre,FileWritePre    *.gpg   '[,']!gpg --default-recipient-self -ae
   " Undo the encryption so we are back in the normal text, directly
   " after the file has been written.
   autocmd BufWritePost,FileWritePost    *.gpg   u
augroup END
"}}}

function! Commente() range
   exe a:firstline.",".a:lastline."s/^/# /"
endfunction

function! Decommente() range
   exe a:firstline.",".a:lastline."s/^# //e"
endfunction

" Syntax Highlighting
set synmaxcol=4000
au BufRead,BufNewFile *.bdf set filetype=silk "Silk Performer bdf
au BufRead,BufNewFile *.bdh set filetype=silk "Silk Performer bdh
au BufRead,BufNewFile *.lj set filetype=lj "lj
au BufRead,BufNewFile *.loj set filetype=loj "loj
au BufRead,BufNewFile *.log set filetype=log "log
au BufRead,BufNewFile *.txt set filetype=dnktxt
au BufRead,BufNewFile *.cfg set filetype=dnktxt
au BufRead,BufNewFile *.cnf set filetype=dnktxt
au BufRead,BufNewFile *.conf set filetype=dnktxt

"au BufRead,BufNewFile *.lj hi sql guifg=white guibg=red ctermfg=white ctermbg=red
"au BufRead,BufNewFile *.lj syn match sql /misos/

"call Code()
set cursorline
"hi CursorLine cterm=none ctermfg=none ctermbg=233
set cursorcolumn
"hi CursorColumn cterm=none ctermfg=none ctermbg=232
"
"set foldmethod=syntax
"set foldlevel=1000

map <C-left> :tabp<CR>
map <C-right> :tabn<CR>

map <CTRL-V><CTRL-PAGEUP> :tabp<CR>
map <CTRL-V><CTRL-PAGEDOWN> :tabn<CR>


function! StatusLineColor(mode)
	if a:mode == 'i'
		hi User1 ctermbg=red
		hi User2 ctermbg=red
		hi User3 ctermbg=red
	elseif a:mode == 'r'
		hi User1 ctermbg=227
		hi User2 ctermbg=227
		hi User3 ctermbg=227
	else
		hi User1 ctermbg=lightgray
		hi User2 ctermbg=lightgray
		hi User3 ctermbg=lightgray
	endif
endfunction

function! StatusLineNormal()
	hi User1 ctermbg=lightgray
	hi User2 ctermbg=lightgray
	hi User3 ctermbg=lightgray
endfunction
au InsertEnter * call StatusLineColor(v:insertmode)
au InsertChange * call StatusLineColor(v:insertmode)
au InsertLeave * call StatusLineNormal()

set timeoutlen=100 ttimeoutlen=0
set modeline

set iskeyword+=/
set iskeyword+=-
"set mouse=a

set nu
" bug, se quita!
"match NonText '^\t\+'

" experimental clipboard
"set clipboard=unnamedplus
"set clipboard=unnamed
